<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::resource('candidates', 'CandidatesController');

Route::get('/hello',function (){
    return 'Hello Larevel';
});

Route::get('/student/{id}',function ($id='No student found'){
    return 'We got student with id '.$id;
});

Route::get('/car/{id?}',function ($id=null){
    //בדיקה האם התקבל משתנה ID
    if(isset($id)){
        //TODO: validate for intger
        return "we got car $id";
    }
    else{
        return 'we need the id to find your car';
    }
});

Route::get('/comment/{id}', function ($id) {
    return view('comment', compact('id'));
});

Route::get('/users/{name?}{email}/', function ($email=null,$name='name missing') {
       return view('ex5',compact('email','name'));
});